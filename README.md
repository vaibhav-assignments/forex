# Forex

> Forex is a currency conversion app that would help users to get desired currency conversion on the go.

## Table of Contents

- [Preview](#preview)
- [Tech Stack](#tech-stack)
- [Installation](#installation)
    - [Clone](#clone)
    - [Setup](#setup)
    - [DB Initialization](#db-initialization)
- [Build Configurations](#build-configurations)
    - [Local build](#local-build)
    - [Production build (Client)](#production-build-client)
- [Testing (Client)](#testing-client)
- [Unit Tests & Performance Metrics](#unit-tests-performance-metrics)

## Preview

### Mobile View

![mobile view 1](https://app-forex.netlify.app/images/mobile.png)

### Desktop View

![desktop view](https://app-forex.netlify.app/images/desktop.png)

## Tech Stack - MERN

- **Frontend**
    - **UI Design** - ReactJS
    - **State Management** - Redux
    - **Unit Testing** - React Testing Library

- **Backend**
    - **Framework** - NodeJs
    - **Middleware** - ExpressJS
    - **DB ORM** - MongooseJS
    - **DB** - MongoDB

## Installation

Make sure you have the following software installed:
- `git`
- `nodejs`
- `npm`
- `mongoDB`

### Clone

- Clone this repo to your local machine using `https://gitlab.com/VaibhavVinayak/forex.git`

### Setup

- Using `npm`

```shell
$ git clone https://gitlab.com/VaibhavVinayak/forex.git
$ cd forex
$ npm run dep
```

### DB Initialization

> it will insert 4 currencies without any inital exchange rate. Don't worry! server will handle on its own.

```shell
$ mongo
$ db.currencies.insert([ { "symbol": "INR", "label": "Indian Rupee" }, { "symbol": "CAD", "label": "Canadian Dollar" }, { "symbol": "AUD", "label": "Australian Dollar" }, { "symbol": "JPY", "label": "Japanese Yen" } ] )
```

## Build Configurations

### Local build

>Runs the client and server in the development mode. Open [http://localhost:3000](http://localhost:3000) to view client in the browser.

```shell
$ npm start
```

### Production build (Client)

>Creates a production ready build. You can use a production server to serve this build.

```shell
$ cd client
$ npm run build
```

## Testing (Client)

>Runs the unit test cases written for the application.

```shell
$ cd client
$ npm test -- --coverage --watchAll
```

## Unit Tests & Performance Metrics

### Code coverage

>Actual coverage is higher than the screenshot due to a know issue in jest with newer version of react-scripts when run with `--coverage` flag

![Code Coverage](https://app-forex.netlify.app/images/coverage.png)


### Lighthouse Score

![lighthouse score](https://app-forex.netlify.app/images/lighthouse.png)