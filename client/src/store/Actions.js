import {
  GET_CURRENCIES_REQUEST,
  GET_CURRENCIES_SUCCESS,
  GET_CURRENCIES_FAILED,
  REFRESH_EXCHANGE_REQUEST,
  REFRESH_EXCHANGE_SUCCESS,
  REFRESH_EXCHANGE_FAILED,
} from "./ActionTypes";

/**
 * @description Dispatches an action for fetching currencies
 */
export const getCurrenciesRequest = () => ({ type: GET_CURRENCIES_REQUEST });

/**
 * @description Dispatches an action for successfully getting currencies
 * @param payload Success response from the server
 */
export const getCurrenciesSuccess = (payload) => ({
  type: GET_CURRENCIES_SUCCESS,
  payload,
});

/**
 * @description Dispatches an action when api fails fetching currencies
 * @param payload Error recieved from the server
 */
export const getCurrenciesFailed = (payload) => ({
  type: GET_CURRENCIES_FAILED,
  payload,
});

/**
 * @description Dispatches an action for refreshing currency exchange rates
 */
export const refreshExchangeRequest = () => ({
  type: REFRESH_EXCHANGE_REQUEST,
});

/**
 * @description Dispatches an action for successfully getting updated currency exchange rates
 * @param payload Success response from the server
 */
export const refreshExchangeSuccess = (payload) => ({
  type: REFRESH_EXCHANGE_SUCCESS,
  payload,
});

/**
 * @description Dispatches an action when api fails refreshing currency exchange rates
 * @param payload Error recieved from the server
 */
export const refreshExchangeFailed = (payload) => ({
  type: REFRESH_EXCHANGE_FAILED,
  payload,
});
