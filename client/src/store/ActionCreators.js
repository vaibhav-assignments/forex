import Axios from "axios";
import {
  getCurrenciesRequest,
  getCurrenciesSuccess,
  getCurrenciesFailed,
  refreshExchangeRequest,
  refreshExchangeSuccess,
  refreshExchangeFailed,
} from "./Actions";

/**
 * @description Fetches the currency data from API
 */
export const requestCurrencies = () => (dispatch) => {
  dispatch(getCurrenciesRequest());
  Axios.get("/api/currencies")
    .then((res) => dispatch(getCurrenciesSuccess(res.data)))
    .catch((err) => dispatch(getCurrenciesFailed(err?.response?.data || err)));
};

/**
 * @description Fetches the currency data from API
 */
export const requestRefreshExchange = () => (dispatch) => {
  dispatch(refreshExchangeRequest());
  Axios.get("/api/exchange")
    .then((res) => dispatch(refreshExchangeSuccess(res.data)))
    .catch((err) =>
      dispatch(refreshExchangeFailed(err?.response?.data || err))
    );
};
