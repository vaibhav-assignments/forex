import {
  GET_CURRENCIES_REQUEST,
  GET_CURRENCIES_SUCCESS,
  GET_CURRENCIES_FAILED,
  REFRESH_EXCHANGE_REQUEST,
  REFRESH_EXCHANGE_SUCCESS,
  REFRESH_EXCHANGE_FAILED,
} from "./ActionTypes";

const initialState = {
  isLoading: false,
  isError: false,
  error: null,
  currencies: null,
};

/**
 * @description Reducer required for creating the store
 * @param {Object} state Initial state for the store
 * @param {Object} action Action object containing type and payload
 */
const Reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_CURRENCIES_REQUEST:
      return { isLoading: true };
    case GET_CURRENCIES_SUCCESS:
      return { isLoading: false, currencies: payload };
    case GET_CURRENCIES_FAILED:
      return { isLoading: false, isError: true, error: payload };
    case REFRESH_EXCHANGE_REQUEST:
      return { isLoading: true };
    case REFRESH_EXCHANGE_SUCCESS:
      return { isLoading: false, currencies: payload };
    case REFRESH_EXCHANGE_FAILED:
      return { isLoading: false, isError: true, error: payload };
    default:
      return state;
  }
};

export default Reducer;
