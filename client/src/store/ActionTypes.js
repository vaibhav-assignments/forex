/**
 * @description Action Type for fetching currency data
 */
export const GET_CURRENCIES_REQUEST = "GET_CURRENCIES_REQUEST";

/**
 * @description Action Type for successfully getting currency data
 */
export const GET_CURRENCIES_SUCCESS = "GET_CURRENCIES_SUCCESS";

/**
 * @description Action Type when API fails to fetch currency data
 */
export const GET_CURRENCIES_FAILED = "GET_CURRENCIES_FAILED";
/**
 * @description Action Type for refreshing currency data
 */
export const REFRESH_EXCHANGE_REQUEST = "REFRESH_EXCHANGE_REQUEST";

/**
 * @description Action Type for successfully getting the updated currency data
 */
export const REFRESH_EXCHANGE_SUCCESS = "REFRESH_EXCHANGE_SUCCESS";

/**
 * @description Action Type when API fails refreshing currency data
 */
export const REFRESH_EXCHANGE_FAILED = "REFRESH_EXCHANGE_FAILED";
