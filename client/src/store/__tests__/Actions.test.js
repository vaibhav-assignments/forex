import {
  GET_CURRENCIES_REQUEST,
  GET_CURRENCIES_SUCCESS,
  GET_CURRENCIES_FAILED,
  REFRESH_EXCHANGE_REQUEST,
  REFRESH_EXCHANGE_SUCCESS,
  REFRESH_EXCHANGE_FAILED,
} from "../ActionTypes";
import {
  getCurrenciesRequest,
  getCurrenciesSuccess,
  getCurrenciesFailed,
  refreshExchangeRequest,
  refreshExchangeSuccess,
  refreshExchangeFailed,
} from "../Actions";

describe("Actions", () => {
  it("should create an action to dispatch GET_CURRENCIES_REQUEST", () => {
    const expectedAction = {
      type: GET_CURRENCIES_REQUEST,
    };
    expect(getCurrenciesRequest()).toEqual(expectedAction);
  });
  it("should create an action to dispatch GET_CURRENCIES_SUCCESS", () => {
    const expectedAction = {
      type: GET_CURRENCIES_SUCCESS,
      payload: {},
    };
    expect(getCurrenciesSuccess({})).toEqual(expectedAction);
  });
  it("should create an action to dispatch GET_CURRENCIES_FAILED", () => {
    const expectedAction = {
      type: GET_CURRENCIES_FAILED,
      payload: {},
    };
    expect(getCurrenciesFailed({})).toEqual(expectedAction);
  });
  it("should create an action to dispatch REFRESH_EXCHANGE_REQUEST", () => {
    const expectedAction = {
      type: REFRESH_EXCHANGE_REQUEST,
    };
    expect(refreshExchangeRequest()).toEqual(expectedAction);
  });
  it("should create an action to dispatch REFRESH_EXCHANGE_SUCCESS", () => {
    const expectedAction = {
      type: REFRESH_EXCHANGE_SUCCESS,
      payload: {},
    };
    expect(refreshExchangeSuccess({})).toEqual(expectedAction);
  });
  it("should create an action to dispatch REFRESH_EXCHANGE_FAILED", () => {
    const expectedAction = {
      type: REFRESH_EXCHANGE_FAILED,
      payload: {},
    };
    expect(refreshExchangeFailed({})).toEqual(expectedAction);
  });
});
