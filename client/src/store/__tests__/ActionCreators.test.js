import mockAxios from "axios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import {
  GET_CURRENCIES_REQUEST,
  GET_CURRENCIES_SUCCESS,
  GET_CURRENCIES_FAILED,
  REFRESH_EXCHANGE_REQUEST,
  REFRESH_EXCHANGE_SUCCESS,
  REFRESH_EXCHANGE_FAILED,
} from "../ActionTypes";
import { requestCurrencies, requestRefreshExchange } from "../ActionCreators";

jest.mock("axios");
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("ActionCreators", () => {
  it("Fetch Currencies API returns currency data", async () => {
    const mockData = {
      currencies: [
        {
          symbol: "INR",
          exchangeRates: {
            CAD: 0.0178915065,
            JPY: 1.4512333003,
            AUD: 0.0187948499,
          },
          updatedOn: "2020-09-06T09:10:01.974Z",
          label: "Indian Rupee",
          _id: "5f53e8b2cb99d0220d0e45e3",
        },
      ],
    };
    const store = mockStore(mockData);
    const expectedActions = [
      { type: GET_CURRENCIES_REQUEST },
      { type: GET_CURRENCIES_SUCCESS, payload: mockData },
    ];
    mockAxios.get.mockResolvedValue({ data: mockData });
    await store.dispatch(requestCurrencies());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it("Fetch Currencies API returns error", async () => {
    const err = { response: { data: "Network error" } };
    const store = mockStore({});
    const expectedActions = [
      { type: GET_CURRENCIES_REQUEST },
      { type: GET_CURRENCIES_FAILED, payload: err },
    ];
    mockAxios.get.mockRejectedValue(err);
    await store.dispatch(requestCurrencies());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it("Refresh Exchange Rates API returns currency data", async () => {
    const mockData = {
      currencies: [
        {
          symbol: "INR",
          exchangeRates: {
            CAD: 0.0178915065,
            JPY: 1.4512333003,
            AUD: 0.0187948499,
          },
          updatedOn: "2020-09-06T09:10:01.974Z",
          label: "Indian Rupee",
          _id: "5f53e8b2cb99d0220d0e45e3",
        },
      ],
    };
    const store = mockStore(mockData);
    const expectedActions = [
      { type: REFRESH_EXCHANGE_REQUEST },
      { type: REFRESH_EXCHANGE_SUCCESS, payload: mockData },
    ];
    mockAxios.get.mockResolvedValue({ data: mockData });
    await store.dispatch(requestRefreshExchange());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it("Refresh Exchange Rates API returns error", async () => {
    const err = { response: { data: "Network error" } };
    const store = mockStore({});
    const expectedActions = [
      { type: REFRESH_EXCHANGE_REQUEST },
      { type: REFRESH_EXCHANGE_FAILED, payload: err },
    ];
    mockAxios.get.mockRejectedValue(err);
    await store.dispatch(requestRefreshExchange());
    expect(store.getActions()).toEqual(expectedActions);
  });
});
