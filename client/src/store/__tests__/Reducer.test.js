import {
  GET_CURRENCIES_REQUEST,
  GET_CURRENCIES_SUCCESS,
  GET_CURRENCIES_FAILED,
  REFRESH_EXCHANGE_REQUEST,
  REFRESH_EXCHANGE_SUCCESS,
  REFRESH_EXCHANGE_FAILED,
} from "../ActionTypes";
import Reducer from "../Reducer";

const initialState = {
  isLoading: false,
  isError: false,
  error: null,
  currencies: null,
};

describe("Reducer", () => {
  it("should return the initial state", () => {
    expect(Reducer(undefined, {})).toEqual(initialState);
  });
  it("should handle GET_CURRENCIES_REQUEST", () => {
    expect(Reducer([], { type: GET_CURRENCIES_REQUEST }));
  });
  it("should handle GET_CURRENCIES_SUCCESS", () => {
    expect(Reducer([], { type: GET_CURRENCIES_SUCCESS, payload: [1, 2] }));
  });
  it("should handle GET_CURRENCIES_FAILED", () => {
    expect(Reducer([], { type: GET_CURRENCIES_FAILED }));
  });
  it("should handle REFRESH_EXCHANGE_REQUEST", () => {
    expect(Reducer([], { type: REFRESH_EXCHANGE_REQUEST }));
  });
  it("should handle REFRESH_EXCHANGE_SUCCESS", () => {
    expect(Reducer([], { type: REFRESH_EXCHANGE_SUCCESS, payload: [1, 2] }));
  });
  it("should handle REFRESH_EXCHANGE_FAILED", () => {
    expect(Reducer([], { type: REFRESH_EXCHANGE_FAILED }));
  });
});
