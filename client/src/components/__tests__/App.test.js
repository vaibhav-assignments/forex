import React from "react";
import { render } from "@testing-library/react";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import App from "../App";

const mockStore = configureMockStore([thunk]);

const mockData = {
  isLoading: false,
  isError: false,
  error: null,
  currencies: null,
};

describe("Test: App component", () => {
  it("render App component", () => {
    const store = mockStore(mockData);
    render(
      <Provider store={store}>
        <App />
      </Provider>
    );
  });
});
