import React from "react";
import { render } from "@testing-library/react";
import configureMockStore from "redux-mock-store";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import CurrencyForm from "../CurrencyForm";

const mockStore = configureMockStore([thunk]);

const mockData = {
  isLoading: false,
  isError: false,
  error: null,
  currencies: null,
};

describe("Test: CurrencyForm component", () => {
  it("render CurrencyForm component with isLoading", () => {
    mockData.isLoading = true;
    const store = mockStore(mockData);
    render(
      <Provider store={store}>
        <CurrencyForm />
      </Provider>
    );
  });
  it("render CurrencyForm component with isError", () => {
    mockData.isLoading = false;
    mockData.isError = true;
    mockData.error = "Network Error";
    const store = mockStore(mockData);
    render(
      <Provider store={store}>
        <CurrencyForm />
      </Provider>
    );
  });
  it("render CurrencyForm component normally", async () => {
    mockData.isError = false;
    mockData.error = null;
    mockData.currencies = [
      {
        symbol: "INR",
        exchangeRates: {
          CAD: 0.0178915065,
          JPY: 1.4512333003,
          AUD: 0.0187948499,
        },
        updatedOn: "2020-09-06T09:10:01.974Z",
        label: "Indian Rupee",
        _id: "5f53e8b2cb99d0220d0e45e3",
      },
      {
        symbol: "CAD",
        exchangeRates: {
          INR: 55.8924426103,
          JPY: 81.1129739489,
          AUD: 1.0504900696,
        },
        updatedOn: "2020-09-06T16:45:27.566Z",
        label: "Canadian Dollar",
        _id: "5f53e8b2cb99d0220d0e45e4",
      },
    ];
    const store = mockStore(mockData);
    render(
      <Provider store={store}>
        <CurrencyForm />
      </Provider>
    );
  });
});
