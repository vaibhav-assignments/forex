import React from "react";
import { render } from "@testing-library/react";
import ErrorComponent from "../ErrorComponent";

describe("Test: ErorComponent", () => {
  it("render ErrorComponent and clicking refresh", () => {
    const { getByText } = render(<ErrorComponent />);
    const refreshBtn = getByText(/Refresh/i);
    refreshBtn.click();
  });
});
