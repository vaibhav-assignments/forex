import React, { Fragment } from "react";
import logo from "../assets/logo.svg";
import "./App.css";
import CurrencyForm from "./CurrencyForm";

const App = () => (
  <Fragment>
    <header>
      Currency
      <img src={logo} className="app-logo" alt="logo" />
      Exchange
    </header>
    <div className="form-wrapper">
      <CurrencyForm />
    </div>
  </Fragment>
);

export default App;
