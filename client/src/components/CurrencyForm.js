import React, { useState, useEffect, Fragment } from "react";
import { connect } from "react-redux";
import {
  requestCurrencies,
  requestRefreshExchange,
} from "../store/ActionCreators";
import ErrorComponent from "./ErrorComponent";

const CurrencyForm = ({
  isLoading,
  isError,
  error,
  currencies,
  fetchCurrencies,
  refreshExchange,
}) => {
  /**
   * @usage - fetch currencies on component render
   */
  useEffect(() => {
    fetchCurrencies();
    // eslint-disable-next-line
  }, []);

  const [currency1, setCurrency1] = useState("");
  const [currency2, setCurrency2] = useState("");

  const onSelectHandler = (e) => {
    if (e.target.id === "currency1" && e.target.value !== currency2)
      setCurrency1(e.target.value);
    else if (e.target.id === "currency2" && e.target.value !== currency1)
      setCurrency2(e.target.value);
    else {
      alert("Please choose a different option.");
    }
  };

  const [exchangeRate, setExchangeRate] = useState(null);
  const [exchangeDate, setExchangeDate] = useState(null);
  useEffect(() => {
    if (currency1 !== "" && currency2 !== "" && currencies) {
      setExchangeRate(
        currencies.filter(({ symbol }) => symbol === currency1)[0]
          .exchangeRates[currency2]
      );
      setExchangeDate(
        currencies.filter(({ symbol }) => symbol === currency1)[0].updatedOn
      );
    }
  }, [currency1, currency2, currencies]);

  const formatDate = (date) =>
    `${date?.getDate()}/${date?.getMonth() + 1}/${date?.getFullYear()}`;
  const [amount, setAmount] = useState("");

  const options = currencies
    ? currencies.map(({ symbol, label }, id) => (
        <option value={symbol} key={id}>
          {symbol} - {label}
        </option>
      ))
    : null;
  /**
   * @usage - used to clear entered amount if the user reset the dropdown
   */
  useEffect(() => {
    if (currency1 === "" || currency2 === "") setAmount("");
  }, [currency1, currency2]);

  return isLoading ? (
    <div className="loader" />
  ) : isError ? (
    <div>
      <ErrorComponent
        message={error !== null && !error.includes("html") ? error : undefined}
      />
    </div>
  ) : (
    <Fragment>
      <div className="currency-form">
        <div className="form-group">
          <label className="form-label" htmlFor="currency1">
            From:
          </label>
          <select
            className="form-control"
            id="currency1"
            name="currency1"
            onChange={onSelectHandler}
            value={currency1}
          >
            <option value="">Select any currency</option>
            {options}
          </select>
        </div>
        <div className="form-group">
          <label className="form-label" htmlFor="currency2">
            To:
          </label>
          <select
            className="form-control"
            id="currency2"
            name="currency2"
            onChange={onSelectHandler}
            value={currency2}
          >
            <option value="">Select any currency</option>
            {options}
          </select>
        </div>
        {currency1 !== "" && currency2 !== "" && (
          <div className="exchange-rate-block">
            <span>
              Exchange Rate: {Number.parseFloat(exchangeRate).toFixed(2)} as on{" "}
              {formatDate(new Date(exchangeDate))}
            </span>
            <div>
              <button className="btn refresh-btn" onClick={refreshExchange}>
                Refresh
              </button>
            </div>
          </div>
        )}
        {exchangeRate && exchangeRate && (
          <Fragment>
            <div className="form-group">
              <label htmlFor="input" className="form-label">
                Amount
              </label>
              <input
                type="number"
                className="form-control"
                id="input"
                name="input"
                value={amount}
                onChange={(e) => setAmount(e.target.value)}
              />
            </div>
            <div className="output">
              {(Number(amount) * exchangeRate).toFixed(2)} {currency2}
            </div>
          </Fragment>
        )}
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state) => ({ ...state });

const mapDispatchToProps = (dispatch) => ({
  fetchCurrencies: () => dispatch(requestCurrencies()),
  refreshExchange: () => dispatch(requestRefreshExchange()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyForm);
