import React, { useEffect } from "react";
import PropTypes from "prop-types";
import ErrorImage from "../assets/error.png";

const ErrorComponent = ({ message }) => {
  useEffect(() => {
    const errorComponent = document.getElementById("errorComponent");
    errorComponent.className = "toast";
    setTimeout(() => {
      errorComponent.className = "";
    }, 2000);
  });
  const errorToast = (
    <div id="errorComponent">
      <img src={ErrorImage} alt="error" className="error-icon" />
      <div className="error-message">{message}</div>
    </div>
  );
  return (
    <div>
      <button
        className="btn refresh-btn"
        onClick={() => window.location.reload(false)}
      >
        Refresh
      </button>
      {errorToast}
    </div>
  );
};

ErrorComponent.propTypes = { message: PropTypes.string };

ErrorComponent.defaultProps = {
  message:
    "Oops! something unexpected happened at the server. Please try later...",
};

export default ErrorComponent;
