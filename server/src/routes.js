import { Router } from "express";
import currencyRoutes from "./currency/currency-routes";
import exchangeRoutes from "./exchange/exchange-routes";

const router = Router();

router.use("/currencies", currencyRoutes);
router.use("/exchange", exchangeRoutes);

export default router;
