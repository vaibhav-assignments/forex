import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import routes from "./routes";
import { dbConnect } from "../utils/dbUtil";

dotenv.config();

dbConnect().then(() => console.log("db connected!"));

const app = express();

const PORT = process.env.PORT || 5000;

app.use(cors());
app.use("/api", routes);

app.listen(PORT, () => console.log("server started on port", PORT));
