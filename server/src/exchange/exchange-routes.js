import { Router } from "express";
import Axios from "axios";
import { BASE_API } from "../constants";
import CurrencyController from "../currency/currency-controller";

const exchangeRoutes = Router();

exchangeRoutes.get("/", async (req, res) => {
  try {
    const currencies = await CurrencyController.findCurrencies();
    const data = await Promise.all(
      currencies.map(({ symbol }) =>
        Axios.get(`${BASE_API}?base=${symbol}`)
          .then((res) => res.data)
          .catch((err) => err.message)
      )
    );
    const updates = data.map(({ base, rates }) => {
      Object.keys(rates).map(
        (el) =>
          (!currencies.map(({ symbol }) => symbol).includes(el) ||
            el === base) &&
          delete rates[el]
      );
      const currentCurrency = currencies.filter(
        ({ symbol }) => base === symbol
      );
      const { label, _id } =
        currentCurrency.length > 0 ? currentCurrency[0] : null;
      return {
        symbol: base,
        exchangeRates: rates,
        updatedOn: new Date(),
        label,
        _id,
      };
    });
    CurrencyController.updateRates(updates);
    res.json(updates);
  } catch (e) {
    res.status(500).send(e);
  }
});

export default exchangeRoutes;
