import CurrencyModel from "./currency-model";

export default {
  findCurrencies: async () => await CurrencyModel.find(),
  updateRates: async (updates) =>
    await Promise.all(
      updates.map((update) =>
        CurrencyModel.findByIdAndUpdate(update._id, update)
      )
    ),
};
