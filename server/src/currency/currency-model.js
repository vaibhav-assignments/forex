import { Schema, model } from "mongoose";

const currencySchema = new Schema({
  symbol: { type: String, required: true },
  label: { type: String, required: true },
  exchangeRates: { type: Map, of: Number },
  updatedOn: { type: Date, default: Date.now() },
});

export default model("Currencies", currencySchema);
