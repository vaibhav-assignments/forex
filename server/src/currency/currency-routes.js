import { Router } from "express";

import CurrencyController from "./currency-controller";

const currencyRoutes = Router();

currencyRoutes.get("/", async (req, res) => {
  let currencies = await CurrencyController.findCurrencies();
  if (currencies && currencies.length > 0 && !currencies[0].exchangeRates) {
    currencies = CurrencyController.updateRates();
  }
  res.json(currencies);
});

export default currencyRoutes;
