import mongoose from "mongoose";

export const dbConnect = async () => {
  const dbUrl = process.env.DB_URL;
  const dbConfigOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };
  return await mongoose.connect(dbUrl, dbConfigOptions);
};
